const head = 0xA5;

module.exports = {
    START_SCANNING: Buffer.from([head, 0x60]),
    STOP_SCANNING: Buffer.from([head, 0x65]),
    GET_DEVICE_INFO: Buffer.from([head, 0x90]),
    GET_DEVICE_HEALTH: Buffer.from([head, 0x91]),
    SOFT_RESTART: Buffer.from([head, 0x80])
}
