const { SerialPort } = require('serialport');

const commands = require('./commands.js');

const baudRate = 128000;
const validCallbacks = ['error', 'open', 'point'];

function calcAngle(start, end, n, i, distance){
    let angCorrect = 0;
    if(distance > 0){
        angCorrect = Math.atan(21.8 * ((155.3 - distance) / (155.3 * distance)) / Math.PI / 2);
    }
    let startCorrect = start + angCorrect;
    let endCorrect = end + angCorrect;
    if(endCorrect < startCorrect) startCorrect -= 360;
    if(n == 1) return startCorrect;
    let angle = (endCorrect - startCorrect) / (n - 1) * i + startCorrect;
    if(angle < 0) angle += 360;
    return angle;
}

class YDLIDAR{
    _port;
    _state;
    _callbacks;
    _responseMode;
    _typeCode;
    _messageLength;
    _messageBytesReceived;
    _firstMessage;

    constructor(port){
        this._firstMessage = true;
        this._state = -1;
        this._scanState = 0;
        this._port = new SerialPort({
            path: port,
            baudRate,
            dataBits: 8,
            parity: false,
            stopBits: 1
        });
        this._callbacks = {};
        const self = this;
        this._port.on('open', ()=>{
            if(typeof self._callbacks.open == 'function') self._callbacks.open();
        });
        this._port.on('error', (err)=>{
            if(typeof self._callbacks.error == 'function') self._callbacks.error(err);
        });
        this._port.on('data', (chunk)=>{
            if(this._firstMessage){
                this._firstMessage = false;
                return;
            }
            for(let byte of chunk){
                self._handleByte(self, byte);
            }
        });
    }

    _handleByte(self, byte){
        switch(self._state){
            case -1:
                return;
            case 0:
                if(byte == 0xA5){
                    self._state++;
                }
                break;
            case 1:
                if(byte == 0x5A){
                    self._state++;
                }else{
                    self._state = 0;
                }
                break;
            case 2:
                self._messageLength = byte;
                self._state++;
                break;
            case 3:
                self._messageLength |= (byte << 8);
                self._state++;
                break;
            case 4:
                self._messageLength |= (byte << 16);
                self._state++;
                break;
            case 5:
                self._messageLength |= ((byte & 0b111111) << 24);
                self._responseMode = byte & 0b11;
                self._state++;
                break;
            case 6:
                self._typeCode = byte;
                self._state++;
                self._messageBytesReceived = 0;
                break;
            case 7:
                self._messageBytesReceived++;
                switch(self._typeCode){
                    case 0x81:
                        self._handleScan(self, byte);
                        break;
                }
                break;
        }
    }

    _scanState;
    _scanPacketType;
    _scanNSamples;
    _scanStartAngle;
    _scanEndAngle;
    _scanCheck;
    _scanSample;
    _scanSampleN;
    _handleScan(self, byte){
        switch(self._scanState){
            case 0:
                if(byte == 0xAA){
                    self._scanState++;
                }
                break;
            case 1:
                if(byte == 0x55){
                    self._scanState++;
                }else{
                    self._scanState = 0;
                }
                break;
            case 2:
                self._scanPacketType = byte;
                self._scanState++;
                break;
            case 3:
                self._scanNSamples = byte;
                self._scanState++;
                break;
            case 4:
                self._scanStartAngle = byte;
                self._scanState++;
                break;
            case 5:
                self._scanStartAngle |= (byte << 8);
                self._scanStartAngle = (self._scanStartAngle >> 1) / 64;
                self._scanState++;
                break;
            case 6:
                self._scanEndAngle = byte;
                self._scanState++;
                break;
            case 7:
                self._scanEndAngle |= (byte << 8);
                self._scanEndAngle = (self._scanEndAngle >> 1) / 64;
                self._scanState++;
                break;
            case 8:
                self._scanCheck = byte;
                self._scanState++;
                break;
            case 9:
                self._scanCheck |= (byte << 8);
                self._scanState++;
                self._scanSampleN = 0;
                break;
            case 10:
                self._scanSample = byte;
                self._scanState++;
                break;
            case 11:
                self._scanSample |= (byte << 8);
                const distance = self._scanSample / 4;
                if(typeof self._callbacks.point == 'function') self._callbacks.point(
                    calcAngle(
                        self._scanStartAngle, self._scanEndAngle,
                        self._scanNSamples, self._scanSampleN,
                        distance
                    ),
                    distance
                );
                self._scanSampleN++;
                if(self._scanSampleN >= self._scanNSamples){
                    self._scanState = 0;
                }else{
                    self._scanState--;
                }
                break;
        }
    }

    startScanning(){
        this._state = 0;
        this._port.write(commands.START_SCANNING);
    }

    stopScanning(){
        this._state = -1;
        this._port.write(commands.STOP_SCANNING);
    }

    getDeviceInfo(){
        const self = this;
        this._port.write(commands.GET_DEVICE_INFO);
        return new Promise((resolve, reject)=>{
            self.getInfoCallback = resolve;
        });
    }

    getDeviceHealth(){
        const self = this;
        this._port.write(commands.GET_DEVICE_HEALTH);
        return new Promise((resolve, reject)=>{
            self.getHealthCallback = resolve;
        });
    }

    softRestart(){
        this._port.write(commands.SOFT_RESTART);
    }

    on(event, callback){
        if(!validCallbacks.includes(event)) throw new Error('Invalid event name');
        this._callbacks[event] = callback;
    }
    off(event){
        if(!validCallbacks.includes(event)) throw new Error('Invalid event name');
        this._callbacks[event] = undefined;
    }

    close(){
        const self = this;
        return new Promise((resolve, reject)=>{
            if(self._state >= 0){
                self.stopScanning();
            }
            self._port.drain(()=>{
                self._port.close((err)=>{
                    if(err){
                        reject(err);
                    }else{
                        resolve();
                    }
                });
            })
            
        });
    }
}

module.exports = YDLIDAR;
