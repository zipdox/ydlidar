const YLIDAR = require('./index.js');

const lidar = new YLIDAR('/dev/ttyS0');

lidar.on('open', ()=>{
    console.log('opened');
    lidar.startScanning();
});

lidar.on('error', console.error);
lidar.on('point', (angle, distance)=>{
    console.log(angle, distance);
});
